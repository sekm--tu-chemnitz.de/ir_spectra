from Classes.Spectra import Spectra
import matplotlib.pyplot as plt
import src.tuc_colors as tuc_colors
import src.convert as convert

Al13Fe4_1_0 = Spectra(path='data/1.0.txt', color=tuc_colors.darkred)
Al13Fe4_1_1 = Spectra(path='data/1.1.txt', color=tuc_colors.darkblue)
Al13Fe4_1_2 = Spectra(path='data/1.2.txt', color=tuc_colors.darkgreen)
Al13Fe4_PPh_1_1 = Spectra(path='data/PPh_1.1.txt', color=tuc_colors.blue)
Al13Fe4_PPh_1_2 = Spectra(path='data/PPh_1.2.txt', color=tuc_colors.green)

fig, ax = plt.subplots(figsize=convert.cm2inch(20, 10))    # setup plot object

for spectra in [Al13Fe4_1_0, Al13Fe4_1_1, Al13Fe4_1_2, Al13Fe4_PPh_1_1, Al13Fe4_PPh_1_2]:   # plot the spectra
    ax.plot(spectra.content['Wavenumber[cm-1]'], spectra.content['Absorbance'], c=spectra.color)

ax.set_xlim([4000, 400])    # x-axis limits
ax.set_ylim([0.25, 0])      # y-axis limits
ax.set_xlabel(r'$\mathrm{Wavenumber\quad/\quad cm^{-1}}$')  # x-label
ax.set_ylabel(r'$\mathrm{Adsorbance}$')                     # y-label

fig.tight_layout()
fig.savefig('output/IR_spectra.eps')    # output file
fig.show()  # show plot
