import pandas as pd


class Spectra:
    def __init__(self, path: str, color: object) -> object:
        """
        Class for IR spectra. Given path is read-in using pandas; given color is later used for plot.

        :type path: str
        :type color: object

        :rtype: object
        """
        self.color = color
        self.content = pd.read_csv(path, decimal=',', delim_whitespace=True, dtype=float)
